const express= require('express')
const cors = require('cors') 
const bodyParser = require('body-parser'); 

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use(express.static("./public"));
app.use("/uploads", express.static("uploads"));

MongoClient = module.exports = require("./mongo/index");
Database = module.exports = MongoClient.db("Todolist");

const routers = require("./router/index")

app.get("/", (req, res) => {
  res.status(200).json({
    message: "ok",
  });
});

app.use("/todo", routers);

module.exports = app