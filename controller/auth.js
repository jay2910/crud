const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const config = require("../config/index");
const { register, myprofile, updateMyProfile } = require("../mongo/query/user");
const { emailExists } = require("../mongo/query/user");

const signup = async (req, res) => {
  try {
    const {
      fname,
      age,
      gender,
      email,
      password,
      city,
      state,
      hobbies,
      imageName,
    } = res.shared;

    const id = uuidv4();
    const salt = await bcrypt.genSalt(10);
    const encryptedPassword = await bcrypt.hash(password, salt);

    const myData = {
      id,
      fname,
      age: parseInt(age),
      gender,
      email,
      password: encryptedPassword,
      city,
      state,
      hobbies,
      imagePath: `/uploads/${imageName}`,
    };

    const token = jwt.sign({ id, fname, email }, config.jwt.secretKey, {
      expiresIn: config.jwt.jwtexpiresIn,
    });

    const addData = await register(myData);
    if (addData) {
      res.status(200).json({
        response: "User registration successfully",
        UserId: id,
        token,
      });
    }
  } catch (e) {
    res.status(400).json({
      message: "Oops! Something went wrong",
    });
  }
};

const signin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userExist = await emailExists(email);
    if (userExist) {
      const verifyPassword = await bcrypt.compare(password, userExist.password);
      if (verifyPassword) {
        const token = jwt.sign(
          { id: userExist.id, fname: userExist.fname, email: userExist.email },
          config.jwt.secretKey,
          { expiresIn: config.jwt.jwtexpiresIn }
        );
        res.status(200).json({
          response: "User SignIn successfully",
          UserId: userExist.id,
          token,
        });
      } else {
        res.status(400).json({
          response: "Please Enter Proper Password",
        });
      }
    } else {
      res.status(400).json({
        response: "User Not Found",
      });
    }
  } catch (e) {
    res.status(400).json({
      response: "Oops! Something went wrong",
    });
  }
};

const viewprofile = async (req, res) => {
  try {
    const profile = await myprofile(res.shared.id);
    if (profile) {
      res.status(200).json({
        message: "View User Profile",
        response: profile,
      });
    }
  } catch (e) {
    res.status(400).json({
      message: "Oops! Something went wrong",
    });
  }
};

const updateProfile = async (req, res) => {
  try {
    const updateId = req.params.id;
    const salt = await bcrypt.genSalt(10);

    res.shared.imagePath = `/uploads/${res.shared.imageName}`;
    res.shared.password = await bcrypt.hash(res.shared.password, salt);

    const profile = await myprofile(res.shared.id);

    delete res.shared.id;
    delete res.shared.owneremail;
    delete res.shared.imageName;

    if (profile) {
      if (updateId === profile.id) {
        const updateData = await updateMyProfile(updateId, res.shared);

        if (updateData) {
          res.status(200).json({
            message: "Data successfully Update",
            response: { Id: updateId },
          });
        }
      } else {
        res.status(400).json({
          message: "Please Provide Proper Id",
        });
      }
    }
  } catch (e) {
    res.status(400).json({
      message: "Oops! Something went wrong",
    });
  }
};

module.exports = { signup, signin, viewprofile, updateProfile };
