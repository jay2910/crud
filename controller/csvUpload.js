const csv = require("csvtojson");
const { v4: uuidv4 } = require("uuid");
const addData = require("../mongo/query/data");
const { viewAllData } = require("../mongo/query/user");

const uploadDocument = async (req, res) => {
  const csvFilePath = `uploads/${req.body.csvFile}`;

  let addValue = "";
  try {
    csv()
      .fromFile(csvFilePath)
      .then((jsonObj) => {
        addValue = {
          ownerId: res.shared.id,
          id: uuidv4(),
          value: jsonObj,
        };
      });

    await csv().fromFile(csvFilePath);

    if (Object.keys(addValue).length) await addData(addValue);
    res.status(200).json({
      message: "Data Add Successfully",
    });
  } catch (e) {
    return res.status(400).json({
      status: 400,
      message: "Oops! something went wrong",
    });
  }
};

const getUserWiseData = async (req, res) => {
  let userWiseData = [];
  userWiseData = await viewAllData();
  res.status(200).json({
    message: "Users With Data",
    response: userWiseData,
  });
};

module.exports = { uploadDocument, getUserWiseData };
