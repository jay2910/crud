const http = require('http')
const app = require('./app')

const port = 3000

http.createServer(app).listen(port, (err) => {
  if (err) {
    console.log("Error occured on starting the server");
    console.log(err);
    return;
  }
  console.log(`Server running successfully on port: ${port}`);
});
