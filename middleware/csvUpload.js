const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
    const fileEntension = file.originalname.split(".").pop();
    if (!["csv"].includes(fileEntension)) {
      return cb("only csv are allowed");
    }
    req.body.csvFile = file.originalname;
  },
});
const upload = multer({ storage: storage });

module.exports = upload.single("file");
