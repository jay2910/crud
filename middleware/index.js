const jwt = require("jsonwebtoken");
const config = require("../config/index");
const { emailExists } = require("../mongo/query/user");

const auth = async (req, res, next) => {
  const auth =
    req.headers && req.headers.authorization ? req.headers.authorization : "";

  if (!auth) {
    return res.status(400).json({
      status: 400,
      message: "Authorization Error",
    });
  }

  const Token = auth.split(" ")[1];
  if (!Token) {
    return res.status(400).json({
      status: 400,
      message: "Bad Authorization",
    });
  }
  let decryptToken = "";
  try {
    decryptToken = jwt.verify(Token, config.jwt.secretKey);
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Bad Authorization Token",
    });
  }

  const userExist = await emailExists(decryptToken.email);
  if (!userExist) {
    return res.status(400).json({
      status: 400,
      message: "Oops!, User does not exist",
    });
  }
  res.shared = { id: decryptToken.id, owneremail: decryptToken.email };
  return next();
};

module.exports = auth;
