const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
    const fileEntension = file.originalname.split(".").pop();
    if (!["jpg", "jpeg", "png"].includes(fileEntension)) {
      return cb("only Images are allowed");
    }
    req.body.imageName = file.originalname;
  },
});
const upload = multer({ storage: storage });

module.exports = upload.single("profile");
