const MongoClient = require("mongodb").MongoClient;
let mongoConnection = new MongoClient({});
const URL = "mongodb://localhost:27017";
mongoConnection = new MongoClient(URL, {
  useUnifiedTopology: true,
});
mongoConnection
  .connect()
  .then(() => {
    console.log("Database Connected!");
  })
  .catch((err) => {
    console.log(err);
  });

module.exports = mongoConnection;
