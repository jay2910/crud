const Myconnection = Database.collection("users");

const register = (mydata) => Myconnection.insertOne(mydata);

const myprofile = (id) =>
  Myconnection.findOne({ id: id }, { projection: { _id: 0,}});

const emailExists = (email) => Myconnection.findOne({ email: email });

const updateMyProfile = (id, mydata) =>
  Myconnection.updateOne({ id: id }, { $set: mydata });

const viewAllData = () =>
  Myconnection.aggregate([
    {
      $lookup: {
        from: "userData",
        localField: "id",
        foreignField: "ownerId",
        as: "result",
      },
    },
    {
      $project: {
        _id: 0,
        fname: 1,
        age: 1,
        gender: 1,
        email: 1,
        city: 1,
        state: 1,
        hobbies: 1,
        "result.id": 1,
        "result.value": 1,
      },
    },
  ]).toArray();

module.exports = {
  register,
  myprofile,
  emailExists,
  updateMyProfile,
  viewAllData,
};
