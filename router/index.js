const express = require("express");
const allValidations = require("../validations/commonValidation");
const {
  EmailValidator,
  EmailExist,
  PasswordValidator,
  strongPassword,
} = require("../validations/emailValidation");

const {
  signup,
  signin,
  viewprofile,
  updateProfile,
} = require("../controller/auth");
const { uploadDocument, getUserWiseData } = require("../controller/csvUpload");
const auth = require("../middleware/index");
const uploadImage = require("../middleware/upload");
const csvfileupload = require("../middleware/csvUpload");

const router = express.Router();

router.post(
  "/register",
  uploadImage,
  allValidations,
  EmailValidator,
  EmailExist,
  PasswordValidator,
  strongPassword,
  signup
);

router.post("/login", EmailValidator, PasswordValidator, signin);

router.get("/viewprofile", auth, viewprofile);

router.put(
  "/updateProfile/:id",
  auth,
  uploadImage,
  allValidations,
  EmailValidator,
  EmailExist,
  PasswordValidator,
  strongPassword,
  updateProfile
);

router.post("/upload", auth, csvfileupload, uploadDocument);

router.get("/getAllData", getUserWiseData);

module.exports = router;
