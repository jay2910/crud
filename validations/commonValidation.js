const allValidations = async (req, res, next) => {
  const { fname, age, gender, city, state, hobbies, imageName } = req.body;

  if (!fname || typeof fname == "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a First Name",
    });
  }
  if (typeof fname !== "string") {
    return res.status(400).json({
      status: 400,
      message: "Please provide Proper First Name",
    });
  }
  if (!age || typeof age === "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Age",
    });
  }
  if (typeof fname !== "string") {
    return res.status(400).json({
      status: 400,
      message: "Please provide Proper Age",
    });
  }

  if (!gender || typeof gender === "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Gender",
    });
  }
  if (
    !["MALE", "Male", "male", "FEMALE", "Female", "female"].includes(gender)
  ) {
    return res.status(400).json({
      status: 400,
      message: "Please provide Proper Gender",
    });
  }
  if (!city || typeof city === "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a City",
    });
  }
  if (typeof city !== "string") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Proper City",
    });
  }
  if (!state || typeof state === "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a State",
    });
  }
  if (typeof state !== "string") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Proper State",
    });
  }
  if (!hobbies || typeof hobbies === "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Hobbies",
    });
  }
  if (typeof hobbies !== "string") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Proper hobbies",
    });
  }

  if (!imageName || typeof imageName == "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide profile image",
    });
  }
  res.shared = {
    ...res.shared,
    fname,
    age,
    gender,
    city,
    state,
    hobbies,
    imageName,
  };
  return next();
};

module.exports = allValidations;
