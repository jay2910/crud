const { emailExists } = require("../mongo/query/user");

const EmailValidator = async (req, res, next) => {
  const { email } = req.body;
  if (!email || typeof email == "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Email",
    });
  }
  const emailRegex =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!email.match(emailRegex)) {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Proper Email",
    });
  }

  res.shared = { ...res.shared, email: email };
  return next();
};
const EmailExist = async (req, res, next) => {
  const { email } = req.body;
  const { owneremail } = res.shared;
  if (owneremail) {
    if (email === owneremail) {
      return next();
    } else {
      res.shared = { ...res.shared, email: email };
    }
  }
  const checkEmaiilExist = await emailExists(email);
  if (checkEmaiilExist) {
    return res.status(400).json({
      status: 400,
      message: "Oops! Email Already Exists",
    });
  }
  return next();
};

const PasswordValidator = async (req, res, next) => {
  const { password } = req.body;

  if (!password || typeof password == "undefined") {
    return res.status(400).json({
      status: 400,
      message: "Please provide a Password",
    });
  }

  return next();
};

const strongPassword = (req, res, next) => {
  const { password } = req.body;
  const passwordcheck =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/;
  if (!password.match(passwordcheck)) {
    return res.status(400).json({
      status: 400,
      message:
        "Password should be at least 8 characters with UpperCase, LowerCase, Numbers and Special Character",
    });
  }
  res.shared = { ...res.shared, password: password };
  return next();
};

module.exports = {
  EmailValidator,
  EmailExist,
  strongPassword,
  PasswordValidator,
};
